package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    public TaskStartByIndexRequest(@NotNull final Integer index) {
        this.index = index;
    }

}
