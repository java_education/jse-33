package ru.t1.oskinea.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.system.ServerAboutRequest;
import ru.t1.oskinea.tm.dto.response.system.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(request);

        System.out.println("[APPLICATION]");
        System.out.println("Name: " + response.getApplicationName());
        System.out.println();

        System.out.println("[AUTHOR]");
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("Branch: " + response.getGitBranch());
        System.out.println("Commit Id: " + response.getGitCommitId());
        System.out.println("Committer: " + response.getGitCommitterName());
        System.out.println("E-mail: " + response.getGitCommitterEmail());
        System.out.println("Message: " + response.getGitCommitMessage());
        System.out.println("Time: " + response.getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
