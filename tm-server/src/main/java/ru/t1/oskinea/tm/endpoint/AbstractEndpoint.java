package ru.t1.oskinea.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.api.service.IUserService;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.exception.user.AccessDeniedException;
import ru.t1.oskinea.tm.exception.user.PermissionException;
import ru.t1.oskinea.tm.model.User;

public abstract class AbstractEndpoint {

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new PermissionException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
