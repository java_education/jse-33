package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

}
