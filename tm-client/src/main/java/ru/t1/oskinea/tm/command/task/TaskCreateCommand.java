package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.task.TaskCreateRequest;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @NotNull
    private static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final TaskCreateRequest request = new TaskCreateRequest();
        request.setName(name);
        request.setDescription(description);

        getTaskEndpoint().createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
