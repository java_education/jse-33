package ru.t1.oskinea.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    private String applicationName;

    private String email;

    private String gitBranch;

    private String gitCommitId;

    private String gitCommitterName;

    private String gitCommitterEmail;

    private String gitCommitMessage;

    private String gitCommitTime;

    private String name;

}
