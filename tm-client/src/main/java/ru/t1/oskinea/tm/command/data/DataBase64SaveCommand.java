package ru.t1.oskinea.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.data.DataBase64SaveRequest;
import ru.t1.oskinea.tm.enumerated.Role;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data to base64 file.";

    @NotNull
    private static final String NAME = "data-save-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpoint().saveDataBase64(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
