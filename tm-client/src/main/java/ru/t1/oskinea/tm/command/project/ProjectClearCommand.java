package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @NotNull
    private static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");

        @NotNull final ProjectClearRequest request = new ProjectClearRequest();

        getProjectEndpoint().clearProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
