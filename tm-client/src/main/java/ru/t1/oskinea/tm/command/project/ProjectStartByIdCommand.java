package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.project.ProjectStartByIdRequest;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    private static final String NAME = "project-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setId(id);

        getProjectEndpoint().startProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
