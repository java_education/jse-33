package ru.t1.oskinea.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private String email;

    public UserRegistryRequest(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

}
