package ru.t1.oskinea.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Change password for current user.";

    @NotNull
    private static final String NAME = "user-change-password";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest();
        request.setPassword(password);

        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
