package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.task.TaskGetByProjectIdRequest;
import ru.t1.oskinea.tm.dto.response.task.TaskGetByProjectIdResponse;
import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest();
        request.setProjectId(projectId);

        @NotNull TaskGetByProjectIdResponse response = getTaskEndpoint().getTaskByProjectId(request);
        @NotNull final List<Task> tasks = response.getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
