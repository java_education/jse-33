package ru.t1.oskinea.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.user.UserLockRequest;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Lock user.";

    @NotNull
    private static final String NAME = "user-lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);

        getUserEndpoint().lockUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
