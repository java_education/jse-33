package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);

        getTaskEndpoint().unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
