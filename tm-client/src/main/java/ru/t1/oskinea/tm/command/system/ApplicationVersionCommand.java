package ru.t1.oskinea.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.system.ServerVersionRequest;
import ru.t1.oskinea.tm.dto.response.system.ServerVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Show version info.";

    @NotNull
    private static final String NAME = "version";

    @Override
    public void execute() {
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(request);

        System.out.println("[VERSION]");
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
