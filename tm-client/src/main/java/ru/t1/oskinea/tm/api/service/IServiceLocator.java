package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    @SuppressWarnings("unused")
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

}
