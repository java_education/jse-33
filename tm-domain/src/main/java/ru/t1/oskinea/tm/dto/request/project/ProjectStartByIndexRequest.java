package ru.t1.oskinea.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    public ProjectStartByIndexRequest(@NotNull final Integer index) {
        this.index = index;
    }

}
