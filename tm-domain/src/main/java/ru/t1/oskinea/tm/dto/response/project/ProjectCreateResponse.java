package ru.t1.oskinea.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;
import ru.t1.oskinea.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractResponse {

    @NotNull
    private Project project;

    public ProjectCreateResponse(@NotNull final Project project) {
        this.project = project;
    }

}
