package ru.t1.oskinea.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.endpoint.IAuthEndpoint;
import ru.t1.oskinea.tm.api.endpoint.IUserEndpoint;
import ru.t1.oskinea.tm.command.AbstractCommand;
import ru.t1.oskinea.tm.exception.entity.UserNotFoundException;
import ru.t1.oskinea.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
